/* eslint no-undef: 0 */ // --> OFF
const path = require("path");
const webpack = require("webpack");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const port = process.env.PORT;
const dist = path.resolve(__dirname, "dist");
const workboxPlugin = require("workbox-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");

function createEntryPoints() {
  return { main: ["./src/index.js", "./src/widgets/index.js"] };
}

function createOutput() {
  return {
    filename: "[name].bundle.js",
    path: dist,
    hotUpdateChunkFilename: "hot-update.js",
    hotUpdateMainFilename: "hot-update.json",
    publicPath: process.env.PUBLIC_PATH
  };
}

function createDevServer() {
  return {
    historyApiFallback: true,
    allowedHosts: ["*"],
    host: "0.0.0.0",
    port: port,
    inline: true,
    hot: true,
    writeToDisk: true,
    proxy: {
      changeOrigin: true
    }
  };
}

function createPlugins() {
  if (process.env.WORKER) {
    return [
      new MiniCssExtractPlugin({
        filename: "[name].bundle.css",
        path: dist,
        publicPath: process.env.PUBLIC_PATH
      }),
      new CleanWebpackPlugin(),
      new webpack.DefinePlugin({
        GRAPH_URL: JSON.stringify(process.env.GRAPH_URL),
        DEBUG: process.env.DEBUG,
        NODE_ENV: process.env.NODE_ENV,
        WORKER: JSON.stringify(process.env.WORKER),
        PUBLIC_PATH: JSON.stringify(process.env.PUBLIC_PATH)
      }),
      new workboxPlugin.InjectManifest({
        swSrc: "./src/worker/worker.js",
        swDest: "worker.js"
      }),
      new CopyWebpackPlugin([{ from: "public" }]),
      new HtmlWebpackPlugin({ template: "src/index.html" })
    ];
  } else {
    return [
      new MiniCssExtractPlugin({
        filename: "[name].bundle.css",
        path: dist,
        publicPath: process.env.PUBLIC_PATH
      }),
      new CleanWebpackPlugin(),
      new webpack.DefinePlugin({
        GRAPH_URL: JSON.stringify(process.env.GRAPH_URL),
        DEBUG: process.env.DEBUG,
        NODE_ENV: process.env.NODE_ENV,
        WORKER: JSON.stringify(process.env.WORKER)
      }),
      new CopyWebpackPlugin([{ from: "public" }]),
      new HtmlWebpackPlugin({ template: "src/index.html" })
    ];
  }
}

function createOptimization() {
  return {
    minimizer: [
      new UglifyJSPlugin({
        cache: true,
        parallel: true
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  };
}

function moduleCreationRules() {
  return {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /(node_modules|bower_components)/,
        loader: "babel-loader",
        options: { presets: ["@babel/preset-env", "@babel/preset-react"] }
      },
      {
        test: /\.(sc|sa)ss$/,
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"]
      },
      {
        test: /\.css$/,
        use: [MiniCssExtractPlugin.loader, "css-loader"]
      },
      {
        test: /\.(t|o)tf$/,
        loader: "url-loader",
        options: {
          limit: 10000,
          mimetype: "application/octet-stream"
        }
      },
      {
        test: /\.woff2?$/,
        loader: "url-loader",
        options: {
          limit: 10000,
          mimetype: "application/font-woff"
        }
      },
      {
        test: /\.svg$/,
        use: ["@svgr/webpack"]
      },
      {
        test: /\.(jpg|png|git)$/i,
        loader: "file-loader?name=app/images/[name].[ext]"
      },
      { type: "javascript/auto", test: /\.json$/, loader: "json-loader" },
      { test: /\.worker\.js$/, use: { loader: "worker-loader" } }
    ]
  };
}

function useDevTool() {
  return "source-map";
}

function createConfig() {
  let config = {
    mode: process.env.NODE_ENV,
    devtool: useDevTool(),
    entry: createEntryPoints(),
    output: createOutput(),
    devServer: createDevServer(),
    plugins: createPlugins(),
    optimization: createOptimization(),
    module: moduleCreationRules(),
    resolve: {
      modules: ["src", "node_modules"],
      extensions: [".json", ".js", ".jsx", ".mjs", ".gql", ".graphql"]
    }
  };
  return config;
}

module.exports = createConfig();
