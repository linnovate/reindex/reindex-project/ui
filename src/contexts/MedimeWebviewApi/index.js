import React, { createContext, useContext } from "react";

export const AppifyContext = createContext(null);

const checkAppify = () => {
  console.debug("Appify: checking appify");
  if (!window.appify || window.appify === {}) {
    console.debug("Appify: not found");
    return false;
  }
  console.debug("Appify: found");
  return true;
};

const getDeviceInstanceKey = () => {
  if (!checkAppify()) return;
  return window.appify.getDeviceInstanceKey();
};

const showNotification = ({
  firstName,
  lastName,
  emergencyContacts,
  medicalReport,
  lockScreenNotification,
}) => {
  const result = JSON.stringify({
    firstName,
    lastName,
    emergencyContacts,
    medicalReport,
    lockScreenNotification,
  });
  console.debug(result);
  if (!checkAppify()) return;
  try {
    window.appify.showNotification(result);
  } catch (err) {
    console.debug("couldnt set the notification");
  }
};

const getJsonData = () => {
  if (!checkAppify()) return;
  const parsedJson = JSON.parse(window.appify.getJsonData());
  return parsedJson;
};

const helloWorld = () => {
  if (!checkAppify()) return;
  return true;
};

export const AppifyApiMethodsProvider = ({ children }) => (
  <AppifyContext.Provider
    value={{ helloWorld, getDeviceInstanceKey, showNotification, getJsonData }}
  >
    {children}
  </AppifyContext.Provider>
);

export const useAppify = () => useContext(AppifyContext);
