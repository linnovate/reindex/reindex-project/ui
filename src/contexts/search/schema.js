import React from "react";
import gql from "graphql-tag";

export const SEARCH = gql`
  query search($query: JSON) {
    search(query: $query) {
      profiles {
        _id
        firstName
        lastName
        lastUpdated
        lastLogin
        email
        medicalReport {
          overallMedicalCondition
          significantMedicalEvents {
            date
            description
          }
          sensitivities {
            cause
            effect
            severity
          }
          regularMedications {
            name
            frequency {
              dateFormat
              interval
            }
          }
          bloodType
        }
        groups {
          _id
          createdDate
          label
          members {
            profile {
              _id
              firstName
              lastName
            }
            role
          }
        }
      }
    }
  }
`;
