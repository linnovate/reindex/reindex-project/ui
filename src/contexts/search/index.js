import React, { useContext, createContext } from "react";
import { useLazyQuery } from "@apollo/react-hooks";
import { SEARCH } from "./schema";

export const SearchContext = createContext();

export const SearchMethodsProvider = ({ children }) => {
  const useSearch = useLazyQuery(SEARCH);

  return (
    <SearchContext.Provider
      value={{
        useSearch,
      }}
    >
      {children}
    </SearchContext.Provider>
  );
};

export const useSearchMethods = () => useContext(SearchContext);
