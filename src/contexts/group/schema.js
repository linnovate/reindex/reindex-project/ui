import React from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";
import {
  basicProfileData,
  emergencyContacts,
  activation
} from "../profile/schema";

export const groups = `
	groups {
		_id
		label
		createdDate
		members{
			role
			approved
			profile {
				_id
				firstName
				lastName
				email
				gender
				familyStatus
				phoneNumber
				dateOfBirth
				lockScreenNotification
				updatedAt
                createdAt
				lastLogin
				emergencyContacts{
					name
					phoneNumber
					relation
				}
				activation {
					isActive
				}
			}
		}
	}
`;
