import React, { createContext, useContext, useEffect } from "react";
import { useMutation } from "@apollo/react-hooks";

import { SEND_NOTIFICATION } from "./schema";

export const NotificationContext = createContext();

const usePushNotifications = () => {
  const [sendNotification, outcome] = useMutation(SEND_NOTIFICATION);
  const func = ({ uid, title, body }) =>
    sendNotification({
      variables: { data: { identity: uid, custom: { fcm: { title, body } } } },
    });
  return [func, outcome];
};

export const NotificationsMethodsProvider = ({ children }) => {
  return (
    <NotificationContext.Provider value={{ usePushNotifications }}>
      {children}
    </NotificationContext.Provider>
  );
};

export const useNotificationsMethods = () => useContext(NotificationContext);
