import gql from "graphql-tag";

export const SEND_NOTIFICATION = gql`
  mutation sendNotification($data: notificationInput!) {
    sendNotification(data: $data) {
      message
    }
  }
`;
