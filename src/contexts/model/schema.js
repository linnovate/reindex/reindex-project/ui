import React from "react";
import { useMutation } from "@apollo/react-hooks";
import gql from "graphql-tag";

const overallMedicalCondition = `
	overallMedicalCondition
`;

const significantMedicalEvents = `
	significantMedicalEvents {
		date
		description
	}
`;

const sensitivities = `
	sensitivities {
		cause
		effect
		severity
	}
`;

const regularMedications = `
	regularMedications {
		name
		frequency {
			dateFormat
			interval
		}
	}
`;

const bloodType = `
	bloodType
`;

export const basicMedicalReportType = `
	medicalReport {
		_id
		${overallMedicalCondition}
		${significantMedicalEvents}
		${sensitivities}
		${regularMedications}
		${bloodType}
	}
`;

export const UPDATE_MODEL = gql`
  mutation updateFamilyMemberMedicalReport(
    $id: String!
    $data: medicalReportInput!
  ) {
    updateFamilyMemberMedicalReport(id: $id, data: $data) {
		${overallMedicalCondition}
		${significantMedicalEvents}
		${sensitivities}
		${regularMedications}
		${bloodType}
    }
  }
`;
