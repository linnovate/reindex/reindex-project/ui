import React, { createContext, useContext } from "react";
import { UPDATE_MODEL } from "./schema";
import { useMutation } from "@apollo/react-hooks";

export const ModelContext = createContext(null);

export const ModelMethodsProvider = ({ children }) => {
  const updateMedicalReport = useMutation(UPDATE_MODEL);
  return (
    <ModelContext.Provider value={{ updateMedicalReport }}>
      {children}
    </ModelContext.Provider>
  );
};

export const useModelMethods = () => useContext(ModelContext);
