import { ApolloProvider } from "@apollo/react-hooks";
import { InMemoryCache } from "apollo-cache-inmemory";
//import ApolloClient, { ApolloLink } from "apollo-boost";
import { ApolloClient } from "apollo-client";
import { ApolloLink } from "apollo-link";
import { setContext } from "apollo-link-context";
import { HttpLink } from "apollo-link-http";
import React from "react";

class Graph {
  constructor({ apiUrl }) {
    this.apiUrl = apiUrl;
    this.listeners = [];
    this.initClient();
  }
  initClient() {
    const cache = new InMemoryCache();
    // const link = new HttpLink({
    //   uri: 'http://localhost:4000/'
    // });

    const httpLink = new HttpLink({
      uri: this.apiUrl,
      credentials: "same-origin",
      fetchOptions: { redirect: "follow" },
      fetch,
    });
    const cleanTypeName = new ApolloLink((operation, forward) => {
      if (operation.variables) {
        const omitTypename = (key, value) =>
          key === "__typename" || key === "_id" ? undefined : value;
        operation.variables = JSON.parse(
          JSON.stringify(operation.variables),
          omitTypename
        );
      }
      return forward(operation).map((data) => data);
    });

    const authLink = setContext((_, { headers }) => {
      return { headers: { ...headers } };
    });
    this.client = new ApolloClient({
      link: ApolloLink.from([cleanTypeName, authLink, httpLink]),
      cache,
    });
  }
  static getInstace() {
    if (this.instance == null)
      this.instance = new Graph({ apiUrl: GRAPH_URL || "/graphql" });
    return this.instance;
  }
}

const GraphProvider = ({ children }) => {
  return (
    <ApolloProvider client={Graph.getInstace().client}>
      {children}
    </ApolloProvider>
  );
};

export default GraphProvider;
