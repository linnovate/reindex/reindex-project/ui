import React from "react";
import { basicMedicalReportType } from "../model/schema";
import { groups } from "../group/schema";
import gql from "graphql-tag";

export const basicProfileData = `
	_id
	firstName
	lastName
	email
	gender
	familyStatus
	phoneNumber
	dateOfBirth
	lockScreenNotification
	updatedAt
	createdAt
	lastLogin
`;

export const homeAddress = `
	homeAddress {
	country
	city
	street
	houseNumber
	}
`;

export const appify = `
	appify {
		installed
		lastUsed
	}
`;

export const emergencyContacts = `
	emergencyContacts {
		name
		phoneNumber
		relation
	}
`;
export const activation = `
	activation {
		isActive
	}
`;

export const MY_PROFILE = gql`
  query myProfile {
	  myProfile {
		  ${basicProfileData}
		  ${homeAddress}
		  ${emergencyContacts}
		  ${basicMedicalReportType}
		  ${appify}
		  ${groups}
    }
  }
`;
export const LOCK_SCREEN_PROFILE = gql`
  query myProfile {
	  myProfile {
		  firstName
		  lastName
		  ${basicMedicalReportType}
		  ${emergencyContacts}
		  lockScreenNotification
    }
  }
`;

export const GET_PROFILE = gql`
  query getProfile($id: String!) {
	  getProfile(id: $id) {
		  ${basicProfileData}
		  ${emergencyContacts}
		  ${basicMedicalReportType}
		  ${homeAddress}
    }
  }
`;

export const GET_MY_FAMILY = gql`
  query myProfile {
	  myProfile {
		  ${groups}
    }
  }
`;

export const UPDATE_PROFILE = gql`
  mutation updateProfile($id: String!, $data: profileInput!) {
    updateProfile(id: $id, data: $data) {
	    ${basicProfileData}
	    ${emergencyContacts}
	    ${homeAddress}
	    ${basicMedicalReportType}
    }
  }
`;
