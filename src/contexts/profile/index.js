import React, { useContext, createContext } from "react";
import { useLazyQuery, useMutation } from "@apollo/react-hooks";
import {
  MY_PROFILE,
  LOCK_SCREEN_PROFILE,
  GET_PROFILE,
  GET_MY_FAMILY,
  UPDATE_PROFILE,
} from "./schema";

export const ProfileContext = createContext();

export const ProfileMethodsProvider = ({ children }) => {
  const myProfile = useLazyQuery(MY_PROFILE, {
    fetchPolicy: "no-cache",
    errorPolicy: "ignore",
  });
  const getProfile = useLazyQuery(GET_PROFILE);
  const getMyFamily = useLazyQuery(GET_MY_FAMILY);
  const updateProfile = useMutation(UPDATE_PROFILE);

  return (
    <ProfileContext.Provider
      value={{
        myProfile,
        getProfile,
        getMyFamily,
        updateProfile,
      }}
    >
      {children}
    </ProfileContext.Provider>
  );
};

export const useProfileMethods = () => useContext(ProfileContext);
