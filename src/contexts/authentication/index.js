import { useMutation } from "@apollo/react-hooks";
import React, { createContext, useContext, useEffect } from "react";
import {
  INVITE,
  JOIN,
  LOGIN_USER,
  LOGOUT_USER,
  REGISTER_USER,
  VALIDATE_ACTIVATION_CODE,
  VALIDATE_USER,
} from "./schema";

export const AuthenticationContext = createContext();

export const authRedirect = (uri) => {
  const [
    validateUser,
    { data, loading, called, error },
  ] = useAuthenticationMethods().useAuthenticationValidation;
  useEffect(() => {
    validateUser();
  }, []);
  useEffect(() => {
    if (data) {
      if (data.validateUser) {
        window.location = uri;
      }
    }
  }, [data]);
};

export const loginAction = (token, redirect = null) => {
  if (redirect != null) {
    window.location = redirect;
  }
};

export const inviteAction = (code) => {};

export const AuthenticationMethodsProvider = ({ children }) => {
  const useLogin = useMutation(LOGIN_USER);
  const useRegister = useMutation(REGISTER_USER);
  const useLogout = useMutation(LOGOUT_USER);
  const useInvite = useMutation(INVITE);
  const useJoin = useMutation(JOIN);
  const useValidateActivationCode = useMutation(VALIDATE_ACTIVATION_CODE);
  const useAuthenticationValidation = useMutation(VALIDATE_USER);
  return (
    <AuthenticationContext.Provider
      value={{
        useLogin,
        useLogout,
        useRegister,
        useInvite,
        useJoin,
        useAuthenticationValidation,
        authRedirect,
        useValidateActivationCode,
        loginAction,
      }}
    >
      {children}
    </AuthenticationContext.Provider>
  );
};

export const useAuthenticationMethods = () => useContext(AuthenticationContext);
