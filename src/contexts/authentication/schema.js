import gql from "graphql-tag";

export const REGISTER_USER = gql`
  mutation registerUser($data: registerInput) {
    registerUser(data: $data) {
      token
      message
    }
  }
`;
export const LOGIN_USER = gql`
  mutation loginUser($data: loginInput) {
    loginUser(data: $data) {
      token
      message
    }
  }
`;
export const LOGOUT_USER = gql`
  mutation logout{
    logout
  }
`;

export const VALIDATE_USER = gql`
  mutation validateUser {
    validateUser {
      loggedIn
    }
  }
`;

export const INVITE = gql`
  mutation invite($data: inviteInput!) {
    invite(data: $data) {
      id
      code
    }
  }
`;
export const JOIN = gql`
  mutation join($id: String!, $data: joinInput!) {
    join(id: $id, data: $data) {
      token
    }
  }
`;
export const VALIDATE_ACTIVATION_CODE = gql`
  mutation validateActivationCode($id: String, $code: String) {
    validateActivationCode(id: $id, code: $code)
  }
`;

