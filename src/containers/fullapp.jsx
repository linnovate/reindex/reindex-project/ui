////////import React from "react";
////////import GraphProvider from "../contexts/graph";
////////import Results from "./results";
////////import ModelPage from "./modelPage";
////////import HomePage from "./homePage";
////////import { DrawerContext } from "../contexts/drawer";
////////import { ProfileProvider } from "../contexts/profile";
////////import { SearchProvider } from "../contexts/search";

////////import { ErrorBoundary } from "../HOC/error";
////////import "./fullapp.scss";
////////import useDrawer from "../components/useDrawer";
////////import {
////////  BrowserRouter as Router,
////////  Switch,
////////  Route,
////////  useParams
////////} from "react-router-dom";

////////const SpaFullApp = () => {
////////  return (
////////    <>
////////      <ErrorBoundary>
////////	<GraphProvider>
////////	  <ProfileProvider>
////////	    <DrawerContext.Provider value={useDrawer({ side: "right" })}>
////////	      <SearchProvider>
////////		<Header></Header>
////////		<Router>
////////		  <Switch>
////////		    <Route path="/model/:id">
////////		      <ModelPage />
////////		    </Route>
////////		    <Route path="/results">
////////		      <Results />
////////		    </Route>
////////		    <Route path="/">
////////		      <HomePage />
////////		    </Route>
////////		  </Switch>
////////		</Router>
////////	      </SearchProvider>
////////	    </DrawerContext.Provider>
////////	    <div>Hello world :D</div>
////////	  </ProfileProvider>
////////	</GraphProvider>
////////      </ErrorBoundary>
////////    </>
////////  );
////////};

////////export default SpaFullApp;
