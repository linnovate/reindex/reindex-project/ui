import React, { useState, useEffect, useRef } from "react";

import { useAuthenticationMethods } from "../../contexts/authentication";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";

const InvitePage = () => {
  const [invite, { data }] = useAuthenticationMethods().useInvite;

  const [phoneNumber, setPhoneNuber] = useState(null);
  const [linkToShare, setLinkToShare] = useState(null);
  const [joinCode, setJoinCode] = useState(null);

  const shareLinkElement = useRef(null);

  useEffect(() => {
    if (data) {
      const { id, code } = data.invite;
      setLinkToShare(
        window.location.href.replace("/invite", "") + "/join/" + id
      );
      setJoinCode(code);
    }
  }, [data]);

  const handleChange = (e) => {
    e.persist();
    setPhoneNuber(e.target.value);
  };

  const copyToClipBoard = (e) => {
    const selection = window.getSelection();
    const range = document.createRange();
    range.selectNodeContents(shareLinkElement.current);
    selection.removeAllRanges();
    selection.addRange(range);
    document.execCommand("copy");
    selection.removeAllRanges();
  };

  return (
    <>
      <Box p={"20px"} boxShadow={3} display="flex" flexDirection="column">
        <Typography variant="h5" component="h2">
          Invite
        </Typography>
        <TextField
          name="phoneNumber"
          label="Enter phone number"
          onChange={handleChange}
          fullWidth
        />

        <Button
          style={{ marginTop: "30px", marginBottom: "50px" }}
          color="primary"
          disabled={phoneNumber ? false : true}
          onClick={() => {
            invite({
              variables: {
                data: { phoneNumber },
              },
            });
          }}
          variant="contained"
        >
          submit
        </Button>

        {linkToShare && (
          <div>
            <Typography
              variant="body1"
              component="div"
              style={{ display: "block", marginBottom: "20px" }}
            >
              Join Link:
              <span
                id="share-link"
                ref={shareLinkElement}
                style={{ display: "block", wordWrap: "break-word" }}
              >
                {linkToShare}
              </span>
            </Typography>

            <Typography
              variant="body1"
              component="div"
              style={{ marginBottom: "20px" }}
            >
              Join validation code: {joinCode}
            </Typography>
            <Button variant="outlined" onClick={copyToClipBoard} fullWidth>
              Copy Join Link
            </Button>
          </div>
        )}
      </Box>
    </>
  );
};

export default InvitePage;
