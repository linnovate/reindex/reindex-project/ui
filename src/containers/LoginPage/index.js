import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

import { useAuthenticationMethods } from "../../contexts/authentication";
import { useAppify } from "../../contexts/MedimeWebviewApi";

const LoginPage = ({ pageTitle }) => {
  const [fields, setFields] = useState({ phoneNumber: null, password: null });
  const [
    loginUser,
    { data, loading, error, called },
  ] = useAuthenticationMethods().useLogin;

  const { loginAction } = useAuthenticationMethods();
  const { getDeviceInstanceKey } = useAppify();

  // perform login
  useEffect(() => {
    if (data) {
      const { token, message } = data.loginUser;
      if (token != null) {
        loginAction(token, "/profile");
      }
    }
  }, [data]);

  return (
    <>
      <Box mt={5} p={5} display="flex" flexDirection="column">
        <Typography
          variant="h5"
          component="h1"
          style={{ display: "box", margin: "0px auto 159px auto" }}
        >
          {pageTitle}
        </Typography>
        <TextField
          label="Phone number"
          type="tel"
          onChange={(e) =>
            setFields({ ...fields, phoneNumber: e.target.value })
          }
        />
        <TextField
          label="Password"
          type="password"
          onChange={(e) => setFields({ ...fields, password: e.target.value })}
        />
        <Button
          variant="contained"
          style={{ marginTop: "25px" }}
          color="secondary"
          onClick={() => {
            loginUser({
              variables: {
                data: {
                  ...fields,
                  device: {
                    fcmId: getDeviceInstanceKey(),
                    deviceName: "check",
                  },
                  appify: { installed: window.appify != undefined },
                },
              },
            });
          }}
          disabled={called && !error}
        >
          {loading ? "Loading..." : "Login"}
        </Button>
        {error && <p>{JSON.stringify(error)}</p>}
      </Box>
    </>
  );
};

export default LoginPage;
