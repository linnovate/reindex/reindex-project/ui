import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { arrayToObject } from "../../utils";

import { useAuthenticationMethods } from "../../contexts/authentication";
import { useAppify } from "../../contexts/MedimeWebviewApi";

const RegisterPage = ({ pageTitle, formFields, formFieldsLabels }) => {
  const [fields, setFields] = useState(arrayToObject(formFields.split(",")));

  const [
    registerUser,
    { data, loading, error, called },
  ] = useAuthenticationMethods().useRegister;
  const { getDeviceInstanceKey } = useAppify();

  useEffect(() => {
    if (data) {
      const { token, message } = data.registerUser;
      if (token != null) {
        window.location = "/profile";
      }
    }
  }, [data]);

  const FieldsGenerator = ({ handleChange, formFields, formFieldsLabels }) => {
    const splitFields = formFields;
    const splitLabels = formFieldsLabels;
    if (splitFields && splitFields.length > 0) {
      return splitFields.map((field, index) => {
        return (
          <TextField
            name={field}
            key={index}
            label={splitLabels[index]}
            onChange={(e) => handleChange(e)}
            type={
              field.toLowerCase() === "password"
                ? "password"
                : field.toLowerCase() === "email"
                ? "email"
                : "text"
            }
          ></TextField>
        );
      });
    }
    return <></>;
  };

  return (
    <>
      <Box mt={5} p={5} display="flex" flexDirection="column">
        <Typography
          variant="h5"
          component="h1"
          style={{ display: "box", margin: "0px auto 100px auto" }}
        >
          {pageTitle}
        </Typography>
        {FieldsGenerator({
          handleChange: (e) => {
            setFields({ ...fields, [e.target.name]: e.target.value });
          },
          formFields: formFields.split(","),
          formFieldsLabels: formFieldsLabels.split(","),
        })}
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            registerUser({
              variables: {
                data: {
                  ...fields,
                  device: {
                    fcmId: getDeviceInstanceKey(),
                    deviceName: "check",
                  },
                  appify: { installed: window.appify != undefined },
                },
              },
            });
          }}
          disabled={called && error}
          style={{ marginTop: "50px" }}
        >
          {loading ? "Loading..." : "Register"}
        </Button>
      </Box>
    </>
  );
};
export default RegisterPage;
