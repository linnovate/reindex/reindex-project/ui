import React, { useState, useEffect } from "react";

import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import Select from "@material-ui/core/Select";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import MenuItem from "@material-ui/core/MenuItem";
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker
} from "@material-ui/pickers";
import Box from "@material-ui/core/Box";

import { useProfileMethods } from "../../contexts/profile";
import { useModelMethods } from "../../contexts/model";

import { InputLabel } from "@material-ui/core";
const EditPage = () => {
  const profileId = window.location.pathname.includes("/edit/")
    ? window.location.pathname.replace("/edit/", "")
    : null;

  // Use the myProfile or getProfile(ID) hooks
  const [myProfile, { data, loading, called, error }] =
    profileId == null
      ? useProfileMethods().myProfile
      : useProfileMethods().getProfile;

  const [
    updateProfile,
    { updateData, updateLoading, updateCalled, updateError }
  ] = useProfileMethods().updateProfile;

  const [
    updateMedicalReport,
    { updateModel, modelLoading, modelCalled, modelError }
  ] = useModelMethods().updateMedicalReport;

  const [profile, setProfile] = useState(null);
  const [medicalReport, setMedicalReport] = useState(null);
  const [emergencyContacts, setEmergencyContacts] = useState(null);

  //Componenet did Mount
  useEffect(() => {
    profileId == null
      ? myProfile()
      : myProfile({ variables: { id: profileId } });
  }, []);

  // on Data or Error
  useEffect(() => {
    if (data) {
      if (profileId != null) {
        const {
          medicalReport,
          createdAt,
          lastUpdated,
          lastLogin,
          ...rest
        } = data.getProfile;
        const { emergencyContacts } = rest;
        setProfile(rest);
        setMedicalReport(medicalReport);
        setEmergencyContacts(emergencyContacts);
      } else {
        const { medicalReport, ...rest } = data.myProfile;
        const { emergencyContacts } = rest;
        setProfile(rest);
        setMedicalReport(medicalReport);
        setEmergencyContacts(emergencyContacts);
      }
    }
    if (error) {
      window.location = "/profile";
    }
  }, [data, error]);

  const handleChange = e => {
    e.persist();
    let result = "";
    if (e.target.type === "number") {
      result = parseInt(e.target.value);
    } else {
      result = e.target.value;
    }
    const group = e.target.name.split(":")[0];
    const fieldName = e.target.name.split(":")[1];
    if (group === "profile") {
      setProfile({ ...profile, [fieldName]: result });
    }
    if (group === "medical") {
      console.log(e.target.name);
      setMedicalReport(oldState => {
        if (
          fieldName === "significantMedicalEvents" ||
          fieldName === "regularMedications" ||
          fieldName === "sensitivities"
        ) {
          const fieldIndex = e.target.name.split(":")[2];
          const subFieldValue = e.target.name.split(":")[3];
          const tempArray = oldState[fieldName];
          if (subFieldValue.split(".").length > 1) {
            tempArray[fieldIndex][subFieldValue.split(".")[0]][
              subFieldValue.split(".")[1]
            ] = result;
          } else {
            tempArray[fieldIndex][subFieldValue] = result;
          }
          return { ...medicalReport, [fieldName]: tempArray };
        }
        return { ...medicalReport, [fieldName]: result };
      });
    }
    if (group === "emergencyContacts") {
      const subFieldName = e.target.name.split(":")[2];
      setEmergencyContacts(oldState => {
        let newEmergencyContacts = oldState;

        newEmergencyContacts[fieldName][subFieldName] = result;
        return newEmergencyContacts;
      });
    }
  };
  const handleCheck = e => {
    const group = e.target.name.split(":")[0];
    const fieldName = e.target.name.split(":")[1];
    if (group === "profile") {
      setProfile({ ...profile, [fieldName]: e.target.checked });
    }
    if (group === "medical") {
      setMedicalReport({ ...medicalReport, [fieldName]: e.target.checked });
    }
  };

  const addItem = ({ stateManager, objectHeirarchy, stubObject }) => {
    const split = objectHeirarchy.split(".");
    stateManager(oldState => {
      const newState = { ...oldState };

      newState[split[0]].push(stubObject);
      return newState;
    });
  };
  const removeItem = ({ stateManager, objectHeirarchy }) => {
    const split = objectHeirarchy.split(".");
    stateManager(oldState => {
      const newState = { ...oldState };

      newState[split[0]].pop();
      return newState;
    });
  };

  return (
    <>
      {!loading && profile && (
        <Box p={"20px"} boxShadow={3} display="flex" flexDirection="column">
          <Typography variant="h5" component="h2">
            Edit Profile
          </Typography>
          <TextField
            name="profile:firstName"
            label="First name"
            placeholder={profile?.firstName}
            onChange={handleChange}
            type="text"
          />
          <TextField
            name="profile:lastName"
            label="Last name"
            placeholder={profile?.lastName}
            onChange={handleChange}
            type="text"
          />
          <TextField
            name="profile:email"
            label="Email"
            placeholder={profile?.email}
            onChange={handleChange}
            type="email"
          />
          <FormLabel component="legend" style={{ marginTop: "20px" }}>
            Gender
          </FormLabel>
          <RadioGroup
            name="profile:gender"
            aria-label="gender"
            value={profile?.gender}
            onChange={handleChange}
          >
            <FormControlLabel
              value="female"
              control={<Radio />}
              label="Female"
            />
            <FormControlLabel value="male" control={<Radio />} label="Male" />
            <FormControlLabel value="other" control={<Radio />} label="Other" />
          </RadioGroup>
          <FormLabel component="legend" style={{ marginTop: "20px" }}>
            Martial Status
          </FormLabel>
          <RadioGroup
            name="profile:familyStatus"
            aria-label="familyStatus"
            value={profile?.familyStatus}
            onChange={handleChange}
          >
            <FormControlLabel
              value="single"
              control={<Radio />}
              label="Single"
            />
            <FormControlLabel
              value="relationship"
              control={<Radio />}
              label="In Relationship"
            />
            <FormControlLabel
              value="engaged"
              control={<Radio />}
              label="Engaged"
            />
            <FormControlLabel
              value="married"
              control={<Radio />}
              label="Married"
            />
            <FormControlLabel
              value="divorced"
              control={<Radio />}
              label="divorced"
            />
            <FormControlLabel
              value="widowed"
              control={<Radio />}
              label="widowed"
            />
            <FormControlLabel value="other" control={<Radio />} label="Other" />
          </RadioGroup>
          <TextField
            label="Birthday"
            type="date"
            name="profile:dateOfBirth"
            defaultValue="2017-05-24"
            InputLabelProps={{
              shrink: true
            }}
            onChange={handleChange}
            style={{ marginTop: "20px" }}
          />
          <FormControlLabel
            control={
              <Checkbox
                name="profile:lockScreenNotification"
                checked={profile?.lockScreenNotification}
                onChange={handleCheck}
                color="primary"
                value={profile?.lockScreenNotification}
              />
            }
            label="Lock Screen Notification"
            style={{ marginTop: "20px" }}
          />
          <FormLabel style={{ marginTop: "20px" }}>
            Emergency Contacts:
          </FormLabel>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              addItem({
                stateManager: setProfile,
                objectHeirarchy: "emergencyContacts",
                stubObject: { name: "", phoneNumber: "", relation: "" }
              });
            }}
          >
            Add Contact
          </Button>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              removeItem({
                stateManager: setProfile,
                objectHeirarchy: "emergencyContacts"
              });
            }}
          >
            Remove Contact
          </Button>
          {emergencyContacts?.map((contact, index) => {
            return (
              <div style={{ marginLeft: "20px", marginTop: "10px" }}>
                <FormLabel style={{ marginTop: "15px" }}>
                  Contact #{index + 1}
                </FormLabel>
                <TextField
                  name={`emergencyContacts:${index}:name`}
                  label="Contact Name"
                  placeholder={contact.name}
                  onChange={handleChange}
                  fullWidth
                />
                <TextField
                  name={`emergencyContacts:${index}:phoneNumber`}
                  label="Contact Phone number"
                  placeholder={contact.phoneNumber}
                  onChange={handleChange}
                  fullWidth
                />
                <TextField
                  name={`emergencyContacts:${index}:relation`}
                  label="Contact Relation"
                  placeholder={contact.relation}
                  onChange={handleChange}
                  fullWidth
                />
              </div>
            );
          })}

          <Button
            style={{ marginTop: "30px", marginBottom: "50px" }}
            color="primary"
            onClick={() => {
              const parsedProfile = { ...profile };
              delete parsedProfile.invites;
              updateProfile({
                variables: {
                  id: profile?._id,
                  data: {
                    ...parsedProfile,
                    emergencyContacts: emergencyContacts
                  }
                }
              });
            }}
            variant="contained"
          >
            Save!
          </Button>
        </Box>
      )}
      {medicalReport && (
        <Box p={"20px"} boxShadow={3} display="flex" flexDirection="column">
          <Typography variant="h5" component="h2">
            Edit Medical Report
          </Typography>
          <TextField
            name="medical:overallMedicalCondition"
            label="Overall medical condition"
            placeholder={medicalReport?.overallMedicalCondition}
            onChange={handleChange}
            type="text"
          />
          <InputLabel id="bloodTypeLabel">Blood Type</InputLabel>
          <Select
            name="medical:bloodType"
            labelId="bloodTypeLabel"
            value={medicalReport?.bloodType}
            onChange={handleChange}
          >
            <MenuItem value={"OPOSITIVE"}>OPOSITIVE</MenuItem>
            <MenuItem value={"ONEGATIVE"}>ONEGATIVE</MenuItem>
            <MenuItem value={"APOSITIVE"}>APOSITIVE</MenuItem>
            <MenuItem value={"ANEGATIVE"}>ANEGATIVE</MenuItem>
            <MenuItem value={"ABPOSITIVE"}>ABPOSITIVE</MenuItem>
            <MenuItem value={"ABNEGATIVE"}>ABNEGATIVE</MenuItem>
            <MenuItem value={"BPOSITIVE"}>BPOSITIVE</MenuItem>
            <MenuItem value={"BNEGATIVE"}>BNEGATIVE</MenuItem>
          </Select>
          <FormLabel style={{ marginTop: "20px" }}>
            Significant Medical Events:
          </FormLabel>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              addItem({
                stateManager: setMedicalReport,
                objectHeirarchy: "significantMedicalEvents",
                stubObject: { date: "", description: "" }
              });
            }}
          >
            Add Medical Event
          </Button>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              removeItem({
                stateManager: setMedicalReport,
                objectHeirarchy: "significantMedicalEvents"
              });
            }}
          >
            Remove Medical Event
          </Button>
          {medicalReport?.significantMedicalEvents?.map((event, index) => {
            return (
              <div style={{ marginLeft: "20px", marginTop: "10px" }}>
                <FormLabel style={{ marginTop: "15px" }}>
                  Event #{index + 1}
                </FormLabel>
                <TextField
                  label="Date of the event"
                  type="date"
                  name={`medical:significantMedicalEvents:${index}:date`}
                  defaultValue={event.date}
                  InputLabelProps={{
                    shrink: true
                  }}
                  onChange={handleChange}
                  fullWidth
                  style={{ marginTop: "20px" }}
                />
                <TextField
                  name={`medical:significantMedicalEvents:${index}:description`}
                  label="Description"
                  placeholder={event.description}
                  onChange={handleChange}
                  fullWidth
                />
              </div>
            );
          })}

          <FormLabel style={{ marginTop: "30px" }}>Sensitivities:</FormLabel>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              addItem({
                stateManager: setMedicalReport,
                objectHeirarchy: "sensitivities",
                stubObject: { cause: "", effect: "", severity: 0 }
              });
            }}
          >
            Add Sensitivity
          </Button>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              removeItem({
                stateManager: setMedicalReport,
                objectHeirarchy: "sensitivities"
              });
            }}
          >
            Remove Sensitivity
          </Button>
          {medicalReport?.sensitivities?.map((sensitivity, index) => {
            return (
              <div style={{ marginLeft: "20px", marginTop: "10px" }}>
                <FormLabel style={{ marginTop: "15px" }}>
                  Sensitivity #{index + 1}
                </FormLabel>
                <TextField
                  name={`medical:sensitivities:${index}:cause`}
                  label="Cause"
                  placeholder={sensitivity.cause}
                  onChange={handleChange}
                  fullWidth
                />
                <TextField
                  name={`medical:sensitivities:${index}:effect`}
                  label="Effect"
                  placeholder={sensitivity.effect}
                  onChange={handleChange}
                  fullWidth
                />
                <TextField
                  name={`medical:sensitivities:${index}:severity`}
                  label="Severity"
                  placeholder={sensitivity.severity}
                  onChange={handleChange}
                  type="number"
                  inputProps={{ min: "0", max: "5", step: "1" }}
                  fullWidth
                />
              </div>
            );
          })}
          <FormLabel style={{ marginTop: "30px" }}>
            Regular Medications:
          </FormLabel>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              addItem({
                stateManager: setMedicalReport,
                objectHeirarchy: "regularMedications",
                stubObject: {
                  name: "",
                  frequency: { dateFormat: "", interval: 0 }
                }
              });
            }}
          >
            Add Regular Medication
          </Button>
          <Button
            color="primary"
            variant="outlined"
            onClick={e => {
              removeItem({
                stateManager: setMedicalReport,
                objectHeirarchy: "regularMedications"
              });
            }}
          >
            Remove Regular Medication
          </Button>
          {medicalReport?.regularMedications?.map((medication, index) => {
            return (
              <div style={{ marginLeft: "20px", marginTop: "10px" }}>
                <FormLabel style={{ marginTop: "15px" }}>
                  Medication #{index + 1}
                </FormLabel>
                <TextField
                  name={`medical:regularMedications:${index}:name`}
                  label="Name"
                  placeholder={medication.name}
                  onChange={handleChange}
                  fullWidth
                />
                <TextField
                  name={`medical:regularMedications:${index}:frequency.dateFormat`}
                  label="How often"
                  placeholder={medication.frequency.dateFormat}
                  onChange={handleChange}
                  fullWidth
                />
                <TextField
                  name={`medical:regularMedications:${index}:frequency.interval`}
                  label="Interval"
                  placeholder={medication.frequency.interval}
                  onChange={handleChange}
                  fullWidth
                  type="number"
                />
              </div>
            );
          })}
          <Button
            style={{ marginTop: "30px", marginBottom: "50px" }}
            color="primary"
            onClick={() => {
              const parsedProfile = { ...profile };
              delete parsedProfile.invites;
              updateMedicalReport({
                variables: {
                  id: medicalReport?._id,
                  data: medicalReport
                }
              });
            }}
            variant="contained"
          >
            Save!
          </Button>
        </Box>
      )}
      {loading && <p>loading....</p>}
    </>
  );
};

export default EditPage;
