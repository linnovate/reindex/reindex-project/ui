import React, { useState, useEffect } from "react";
import Typography from "@material-ui/core/Typography";
import { useAuthenticationMethods } from "../../contexts/authentication";
import { useAppify } from "../../contexts/MedimeWebviewApi/index";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Box from "@material-ui/core/Box";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";

const JoinPage = () => {
  const { loginAction } = useAuthenticationMethods();

  const [
    validateActivationCode,
    { data },
  ] = useAuthenticationMethods().useValidateActivationCode;

  const [join, response] = useAuthenticationMethods().useJoin;
  const { getDeviceInstanceKey } = useAppify();

  const [open, setOpen] = React.useState(false);
  const [code, setCode] = useState(null);
  const [fields, setFields] = useState({
    firstName: "",
    lastName: "",
    password: "",
    email: "",
  });

  const id = window.location.pathname.split("/").pop();

  useEffect(() => {
    if (data && data.validateActivationCode) {
      setOpen(true);
    }
  }, [data]);

  useEffect(() => {
    if (response)
      if (response.data) {
        const { token, message } = response.data.join;
        console.log("massage", message, token);
        if (token != null) {
          //insert JWT as login
          //redirect to profile page
          loginAction(token, "/profile");
        }
      }
  }, [response]);
  const handleChange = (e) => {
    e.persist();
    setCode(e.target.value);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <Box p={"20px"} boxShadow={3} display="flex" flexDirection="column">
        <Typography variant="h5" component="h2">
          Join
        </Typography>
        <TextField
          name="code"
          label="Enter verify code"
          onChange={handleChange}
          fullWidth
        />

        <Button
          style={{ marginTop: "30px", marginBottom: "50px" }}
          color="primary"
          disabled={code ? false : true}
          onClick={() => {
            validateActivationCode({
              variables: {
                code,
                id,
              },
            });
          }}
          variant="contained"
        >
          submit
        </Button>
        <Dialog open={open} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">
            Fill your details form to complete the registration
          </DialogTitle>
          <DialogContent>
            <TextField
              autoFocus
              name="firstName"
              margin="dense"
              id="firstName"
              label="First Name"
              fullWidth
              onChange={(e) => {
                console.debug(e.target.name);
                setFields({ ...fields, [e.target.name]: e.target.value });
              }}
            />
            <TextField
              margin="dense"
              name="lastName"
              id="lastName"
              label="Last Name"
              fullWidth
              onChange={(e) => {
                setFields({ ...fields, [e.target.name]: e.target.value });
              }}
            />
            <TextField
              margin="dense"
              name="password"
              id="password"
              label="Password"
              type="password"
              fullWidth
              onChange={(e) => {
                setFields({ ...fields, [e.target.name]: e.target.value });
              }}
            />
            <TextField
              margin="dense"
              name="email"
              id="name"
              label="Email Address"
              type="email"
              fullWidth
              onChange={(e) => {
                setFields({ ...fields, [e.target.name]: e.target.value });
              }}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button
              onClick={() => {
                join({
                  variables: {
                    id,
                    data: {
                      ...fields,
                      code,
                      appify: { installed: window.appify != undefined },
                    },
                  },
                });
              }}
              color="primary"
            >
              Join
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
    </>
  );
};

export default JoinPage;
