import "./index.scss";
import React, { useEffect } from "react";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import { authRedirect } from "../../contexts/authentication";

const LandingPage = () => {
  authRedirect("/profile");
  return (
    <div id="landing-page">
      <Box mt={10} p={"30px"} display="flex" flexDirection="column">
        <Typography
          variant="h5"
          component="h1"
          style={{
            display: "box",
            margin: "0px auto 150px auto",
            width: "fit-content"
          }}
        >
          Welcome to Medime!
        </Typography>
        <Typography variant="body1" component="p">
          You can,
        </Typography>
        <Button variant="contained" color="primary" href="/register">
          Register
        </Button>
        <hr style={{ margin: "10px 0 10px 0" }} />
        <Typography variant="body1" component="p">
          Already part of our family?
        </Typography>
        <Button variant="contained" color="secondary" href="/login">
          Login
        </Button>
      </Box>
    </div>
  );
};

export default LandingPage;
