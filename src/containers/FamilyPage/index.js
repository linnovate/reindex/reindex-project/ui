import React, { useState, useEffect } from "react";
import { useProfileMethods } from "../../contexts/profile";
import Box from "@material-ui/core/Box";
import GroupCard from "../../components/GroupCard";
import Button from "@material-ui/core/Button";

const FamilyPage = ({ pageTitle }) => {
  const [
    getMyFamily,
    { data, loading, error, called },
  ] = useProfileMethods().getMyFamily;
  const [myFamily, setFamily] = useState(null);

  useEffect(() => {
    getMyFamily();
  }, []);

  useEffect(() => {
    if (data) {
      setFamily(data.myProfile);
      if (error) {
        console.error(error);
      }
    }
  }, [data]);

  return (
    <>
      <Box p={"20px"}>
        {loading && <p>loading...</p>}
        <Button
          href="/invite"
          variant="outlined"
          fullWidth
          style={{ marginBottom: "25px" }}
        >
          Invite member
        </Button>
        {!loading &&
          data &&
          myFamily &&
          myFamily.groups.map((group) => (
            <GroupCard
              key={group._id}
              label={group.label}
              createdAt={group.createdDate}
              members={group.members}
              sendPush={true}
            />
          ))}
      </Box>
    </>
  );
};
export default FamilyPage;
