import React, { useEffect, useState, useRef } from "react";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";
import GraphProvider from "../../contexts/graph";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useSearchMethods, SearchMethodsProvider } from "../../contexts/search";
import { TextField, Button } from "@material-ui/core";
import GroupCard from "../../components/GroupCard";
export const ProfileExpansionPanel = ({ profile }) => {
  const getInvites = (currentProfile) => {
    return currentProfile?.groups
      .filter((group) => group.members[0].profile._id === currentProfile._id)
      .map((group) => group.members.length)
      .reduce((a, b) => a + b, 0);
  };
  return (
    <ExpansionPanel>
      <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
        <Typography variant="subtitle1">{`${profile?.firstName} ${profile?.lastName}`}</Typography>
      </ExpansionPanelSummary>
      <ExpansionPanelDetails style={{ display: "block" }}>
        <Typography variant="body1" gutterBottom>
          Email: {profile?.email}
        </Typography>
        <Typography variant="body1" gutterBottom>
          Last Updated: {profile?.lastUpdated}
        </Typography>
        <Typography variant="body1" gutterBottom>
          Last Login: {profile?.lastLogin}
        </Typography>
        <Typography variant="body1">
          Medical Report filled:{" "}
          {JSON.stringify(
            profile?.medicalReport?.overallMedicalCondition != null
          )}
        </Typography>
        <ExpansionPanel>
          <ExpansionPanelSummary>
            Invited Members:{getInvites(profile)}
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            {profile?.groups.map((group, index) => (
              <GroupCard
                key={index}
                label={group.label}
                createdAt={group.createdDate}
                members={group.members}
                sendPush={false}
              />
            ))}
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </ExpansionPanelDetails>
    </ExpansionPanel>
  );
};

export const ProfilesSearch = () => {
  const [search, searchResponse] = useSearchMethods().useSearch;
  const [profiles, setProfiles] = useState(null);
  const [searchSettings, setSearchSettings] = useState({
    from: 0,
    size: 10,
    name: "",
    role: "",
  });

  const viewNext = (number) => {
    setSearchSettings({
      ...searchSettings,
      from: searchSettings.from + number,
    });
  };

  const setSize = (number) => {
    setSearchSettings({ ...searchSettings, size: number });
  };

  useEffect(() => {
    search({
      variables: {
        query: { from: searchSettings.from, size: searchSettings.size },
      },
    });
  }, [searchSettings]);
  useEffect(() => {
    const result = searchResponse?.data?.search?.profiles;
    if (result) setProfiles(result);
  }, [searchResponse?.data]);
  return (
    <>
      {setSearchSettings && (
        <AppBar position="static" color="transparent">
          <Toolbar>
            <Typography variant="h6" style={{ flexGrow: 1 }}>
              Admin Search
            </Typography>
            <TextField
              label="Search Size"
              variant="standard"
              type="number"
              defaultValue={searchSettings?.size}
              onChange={(e) => setSize(parseInt(e.target.value))}
            />
            <Button
              color="primary"
              onClick={(e) => viewNext(Math.ceil(-(searchSettings.size / 2)))}
            >{`Previous ${Math.ceil(searchSettings.size / 2)}`}</Button>
            <Button
              color="primary"
              onClick={(e) => viewNext(Math.ceil(searchSettings.size / 2))}
            >{`Next ${Math.ceil(searchSettings.size / 2)}`}</Button>
          </Toolbar>
        </AppBar>
      )}
      {profiles &&
        profiles.map((profile, index) => {
          return <ProfileExpansionPanel key={index} profile={profile} />;
        })}
    </>
  );
};

export const AdminSearchPage = () => {
  return (
    <Box mt={5} p={5} display="flex" flexDirection="column">
      <GraphProvider>
        <SearchMethodsProvider>
          <ProfilesSearch></ProfilesSearch>
        </SearchMethodsProvider>
      </GraphProvider>
    </Box>
  );
};

