import React, { useState, useEffect } from "react";
import Box from "@material-ui/core/Box";
import Chip from "@material-ui/core/Chip";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import CloseIcon from "@material-ui/icons/Close";
import ProfileCard from "../../components/ProfileCard";

import { useProfileMethods } from "../../contexts/profile";
import { useAppify } from "../../contexts/MedimeWebviewApi";
import { Typography, Button } from "@material-ui/core";

const ProfilePage = ({ pageTitle }) => {
  // Get wether its non ID then its own profile or ID based profile page
  const profileId = window.location.pathname.includes("/profile/")
    ? window.location.pathname.replace("/profile/", "")
    : null;

  // Use the myProfile or getProfile(ID) hooks
  const [myProfile, { data, loading, called, error }] =
    profileId == null
      ? useProfileMethods().myProfile
      : useProfileMethods().getProfile;
  const { showNotification } = useAppify();

  const [profile, setProfile] = useState(null);

  const [
    profileFullnessNotification,
    setProfileFullnessNotification,
  ] = useState(true);
  const [notFullModalState, setNotFullModal] = useState(false);

  const checkProfileDataFullness = () => {
    if (profile) {
      const { firstName, lastName, medicalReport, emergencyContacts } = profile;
      if (
        firstName == null ||
        lastName == null ||
        emergencyContacts.length === 0 ||
        emergencyContacts == undefined
      )
        return false;
      if (
        medicalReport?.overallMedicalCondition == null ||
        medicalReport?.overallMedicalCondition.length === 0
      )
        return false;
      return true;
    }
  };

  //Componenet did Mount
  useEffect(() => {
    profileId == null
      ? myProfile()
      : myProfile({ variables: { id: profileId } });
  }, []);

  // on Data or Error
  useEffect(() => {
    if (data) {
      if (profileId != null) {
        setProfile(data.getProfile);
      } else {
        setProfile(data.myProfile);
        showNotification(data?.myProfile);
      }
    }
    if (error) {
      console.log(error);
    }
  }, [data, error]);

  useEffect(() => {
    if (profile) setProfileFullnessNotification(checkProfileDataFullness());
  }, [profile]);

  return (
    <>
      <Box p={"20px"}>
        {loading && <p>loading...</p>}
        {!loading && data && profile && !profileFullnessNotification && (
          <Chip
            label="Profile isn't full, click here to learn more"
            color="secondary"
            onClick={() => setNotFullModal(true)}
            onDelete={() => setProfileFullnessNotification(true)}
            style={{ width: "100%", marginBottom: "20px" }}
          />
        )}
        {!loading && data && profile && <ProfileCard profile={profile} />}
      </Box>
      {notFullModalState && (
        <Dialog
          onClose={() => setNotFullModal(false)}
          open={notFullModalState}
          fullScreen
        >
          <DialogTitle disableTypography>
            <Typography variant="h6">Your profile isn't complete</Typography>
            <IconButton
              style={{ position: "absolute", top: "10px", right: "20px" }}
              onClick={() => setNotFullModal(false)}
            >
              <CloseIcon />
            </IconButton>
          </DialogTitle>
          <DialogContent dividers>
            <Typography variant="subtitle2">
              The following fields must be full:
            </Typography>
            <List component="div">
              <ListItem>
                <ListItemText primary="First Name" />
              </ListItem>
              <ListItem>
                <ListItemText primary="Last Name" />
              </ListItem>
              <ListItem>
                <ListItemText primary="At least 1 Emergency Contact" />
              </ListItem>
              <ListItem>
                <ListItemText primary="Overall medical condition" />
              </ListItem>
            </List>
          </DialogContent>
          <DialogActions>
            <Button autoFocus href={`/edit/${profile?._id}`} color="primary">
              Alright take me to edit page
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </>
  );
};

export default ProfilePage;
