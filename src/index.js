import "./assets/style/global-styles.scss";
// Render full app function
import SpaFullApp from "./containers/fullapp";
// Setup basic translation
import React from "react";
import ReactDOM from "react-dom";
import T from "i18n-react";
import enTranslation from "./translations/en.translation.json";
if (!global._babelPolyfill) {
  require("babel-polyfill");
}
T.setTexts(Object.assign(T.texts || {}, enTranslation));

window.T_setTexts = function (texts) {
  T.setTexts(Object.assign(T.texts, texts));
};

// Register Widget function
window.registerWidget = function (name, component, widgetStorage = true) {
  console.log(name);
  // create "CRWidgets" object
  !window.CRWidgets && (window.CRWidgets = {});
  !window.WidgetsStorage && (window.WidgetsStorage = []);

  // set CRWidgets render function for each [componentName]
  if (widgetStorage)
    CRWidgets[name] = function (el, settings) {
      if (name === "WidgetsStorage") {
        ReactDOM.unmountComponentAtNode(el);
        ReactDOM.render(React.createElement(component, settings), el);
      } else {
        ReactDOM.unmountComponentAtNode(el);
        window.WidgetsStorage.push({
          name: name,
          component: ReactDOM.createPortal(
            React.createElement(component, settings),
            el
          ),
        });
      }
    };
  else
    CRWidgets[name] = function (el, settings) {
      ReactDOM.unmountComponentAtNode(el);
      ReactDOM.render(React.createElement(component, settings), el);
    };
};

window.renderFullApp = function () {
  ReactDOM.render(<SpaFullApp />, document.getElementById("app"));
};

if ("serviceWorker" in navigator && WORKER === "true") {
  window.addEventListener("load", function () {
    navigator.serviceWorker.register(PUBLIC_PATH + "worker.js").then(
      function (registration) {
        // service worker registration successfull
        console.log(
          "Service worker registratino was successful with scope: ",
          registration.scope
        );
      },
      function (error) {
        // service worker registration failed
        console.log("ServiceWorker registration failed : ", error);
      }
    );
  });
}
