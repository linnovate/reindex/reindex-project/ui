import React from "react";
import LandingPage from "../containers/LandingPage";
import Footer from "../components/Footer";
import Header from "../components/Header";
import LoginPage from "../containers/LoginPage";
import InvitePage from "../containers/InvitePage";
import JoinPage from "../containers/JoinPage";
import RegisterPage from "../containers/RegisterPage";
import ProfilePage from "../containers/ProfilePage";
import EditPage from "../containers/EditPage";
import FamilyPage from "../containers/FamilyPage";
import AdminPage from "../containers/AdminPage";
import { AdminSearchPage } from "../containers/AdminSearchPage";
import GraphProvider from "../contexts/graph";
import { AuthenticationMethodsProvider } from "../contexts/authentication";
import { ProfileMethodsProvider } from "../contexts/profile";
import { ModelMethodsProvider } from "../contexts/model";
import { AppifyApiMethodsProvider } from "../contexts/MedimeWebviewApi";
import { NotificationsMethodsProvider } from "../contexts/notifications";

// Exceptional Widget: holds all the widget in main tree for ContextApi and more
window.registerWidget("WidgetsStorage", WidgetsStorageRenderer);
// Register Widgets Here
window.registerWidget("LoginPage", LoginPage);
window.registerWidget("RegisterPage", RegisterPage);
window.registerWidget("InvitePage", InvitePage);
window.registerWidget("JoinPage", JoinPage);
window.registerWidget("ProfilePage", ProfilePage);
window.registerWidget("EditPage", EditPage);
window.registerWidget("FamilyPage", FamilyPage);
window.registerWidget("LandingPage", LandingPage);
window.registerWidget("Header", Header);
window.registerWidget("Footer", Footer);
window.registerWidget("AdminPage", AdminPage, false);
window.registerWidget("MedimeAdminSearch", AdminSearchPage, false);

// Widgets storage renderer should be once in a page and holds all context providers
export default function WidgetsStorageRenderer(props) {
  const CreateWidgets = () => {
    return window.WidgetsStorage.map((value) => {
      return value.component;
    });
  };
  // Main Widgets tree start here
  return (
    <div>
      <GraphProvider>
        <AppifyApiMethodsProvider>
          <AuthenticationMethodsProvider>
            <ProfileMethodsProvider>
              <ModelMethodsProvider>
                <NotificationsMethodsProvider>
                  <CreateWidgets />
                </NotificationsMethodsProvider>
              </ModelMethodsProvider>
            </ProfileMethodsProvider>
          </AuthenticationMethodsProvider>
        </AppifyApiMethodsProvider>
      </GraphProvider>
    </div>
  );
}
