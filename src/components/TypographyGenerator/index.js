import React from "react";
import Typography from "@material-ui/core/Typography";

const capitalizeSentencalize = text => {
  const sentence = text.replace(/([a-z])([A-Z])/g, "$1 $2");
  return sentence[0].toUpperCase() + sentence.substring(1);
};

export const TypographyField = ({ label, data, style }) => {
  let children = null;
  let value = null;
  if (
    label === "__typename" ||
    label === "role" ||
    label === "_id" ||
    data == null
  )
    return <></>;

  if (Array.isArray(data)) {
    value = null;
    children = (
      <div style={{ width: "100%", display: "flex", flexDirection: "column" }}>
        {data.map(i => (
          <TypographyField
            key={i}
            label={""}
            data={i}
            style={{ marginLeft: "20px", ...style }}
          />
        ))}
      </div>
    );
  } else if (Object.prototype.toString.call(data) === "[object Object]") {
    value = null;
    children = (
      <div style={{ width: "100%", display: "flex", flexDirection: "column" }}>
        {Object.keys(data).map(key => (
          <TypographyField
            key={key}
            label={key}
            data={data[key]}
            style={style}
          />
        ))}
      </div>
    );
  } else {
    value = JSON.stringify(data);
    children = null;
  }

  return (
    <Typography variant="body1" component="div" style={style}>
      {label && capitalizeSentencalize(label) + ":"} {value}
      {children}
    </Typography>
  );
};

export const TypographGenerator = ({ fields, style }) => {
  if (fields === null) return <></>;
  const generatedFields = [];
  Object.keys(fields).forEach(key =>
    generatedFields.push(
      <TypographyField key={key} label={key} data={fields[key]} style={style} />
    )
  );

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      {generatedFields.map(field => field)}
    </div>
  );
};
