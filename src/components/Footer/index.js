import React from "react";

import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";

const Footer = ({ text }) => {
  return (
    <Box
      display="flex"
      border={1}
      flexDirection="column"
      justifyContent="center"
      alignItems="center"
      style={{ width: "100%", padding: "10px", position: "fixed", bottom: "0" }}
    >
      <Typography
        variant="caption"
        component="p"
        style={{ width: "fit-content" }}
        p={"10px"}
      >
        {text}
      </Typography>
    </Box>
  );
};
export default Footer;
