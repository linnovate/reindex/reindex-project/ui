import React, { useState, useEffect } from "react";
import ExpandMore from "@material-ui/icons/ExpandMore";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import ExpansionPanelActions from "@material-ui/core/ExpansionPanelActions";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { TypographGenerator } from "../TypographyGenerator";

import { useNotificationsMethods } from "../../contexts/notifications/index";
const GroupCard = (group) => {
  const { label, createdAt, members, sendPush } = group;
  const [sendNotification, { data, loading, error, called }] = sendPush
    ? useNotificationsMethods().usePushNotifications()
    : [() => {}, { data: null, loading: null, error: null }];
  return (
    <Card key={label}>
      <CardHeader
        title={`Group: ${label[0].toUpperCase() + label.substring(1)}`}
        subheader={`Created at: ${createdAt}, members :${members.length}`}
      />
      <CardContent>
        {members.map((member, index) => (
          <ExpansionPanel key={member.profile._id}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMore />}
              aria-controls={`panel${index}-context`}
              id={`panel${index}-header`}
            >
              <Typography component="h2" variant="h5">
                {`${member.profile.firstName} ${member.profile.lastName}`}
              </Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails
              style={{ display: "flex", flexDirection: "column" }}
            >
              <Typography
                variant="subtitle1"
                component="div"
                style={{ display: "block" }}
              >
                {`Role: ${member.role}`}
              </Typography>
              <Typography
                variant="subtitle1"
                component="div"
                style={{ display: "block", marginBottom: "20px" }}
              >
                {`Approved: ${member.approved}`}
              </Typography>
              <Typography
                variant="body1"
                style={{ display: "block", marginBottom: "20px" }}
              >
                Medical Report filled:{" "}
                {JSON.stringify(
                  member?.medicalReport?.overallMedicalCondition != null
                )}
              </Typography>
              <TypographGenerator fields={member.profile} />
            </ExpansionPanelDetails>
            <ExpansionPanelActions>
              {sendPush && (
                <Button
                  size="small"
                  onClick={() => {
                    sendNotification({
                      uid: member.profile._id,
                      title: "Medime",
                      body: "This is a test message",
                    });
                  }}
                >
                  Send Push
                </Button>
              )}
              <Button size="small" href={`/edit/${member?.profile?._id}`}>
                Edit
              </Button>
            </ExpansionPanelActions>
          </ExpansionPanel>
        ))}
      </CardContent>
    </Card>
  );
};

export default GroupCard;
