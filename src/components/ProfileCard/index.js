import React, { useState, useEffect } from "react";
import IconButton from "@material-ui/core/IconButton";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardHeader from "@material-ui/core/CardHeader";
import Typography from "@material-ui/core/Typography";
import Collapse from "@material-ui/core/Collapse";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import { TypographGenerator } from "../TypographyGenerator";

const ProfileCard = ({ profile, shouldDisplayBackToYourProfile }) => {
  const [medicalReportExpanded, setMedicalReportExpansion] = useState(false);
  const handleExpand = () => {
    setMedicalReportExpansion(!medicalReportExpanded);
  };
  const { medicalReport, ...rest } = profile;

  return (
    <Card>
      <CardHeader
        title={`Profile: ${profile?.firstName}`}
        action={
          <IconButton
            aria-label="settings"
            onClick={() => {
              window.location = `/edit/${profile?._id}`;
            }}
          >
            <MoreVertIcon />
          </IconButton>
        }
      ></CardHeader>
      <CardContent>
        <TypographGenerator
          fields={profile != null || profile != undefined ? rest : null}
        />
      </CardContent>
      <Divider variant="middle" />
      <Button
        onClick={handleExpand}
        fullWidth
        variant="outlined"
        color="primary"
      >
        {medicalReportExpanded ? "Close" : "Open"} Medical Report
      </Button>
      <Collapse in={medicalReportExpanded} timeout="auto" unmountOnExit>
        <CardContent>
          <Typography variant="h5" component="h3">
            Medical Report
          </Typography>
          <TypographGenerator
            fields={
              medicalReport != null || medicalReport != undefined
                ? medicalReport
                : null
            }
          />
        </CardContent>
      </Collapse>
    </Card>
  );
};

export default ProfileCard;
