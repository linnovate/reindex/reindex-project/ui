import React, { useState, useEffect } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { useAuthenticationMethods } from "../../contexts/authentication";

const Header = () => {
  const { useLogout } = useAuthenticationMethods();
  const [invoke] = useLogout;
  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" style={{ marginRight: "15px" }}>
          Medime
        </Typography>
        <Button href="/profile" variant="contained" color="primary">
          Profile
        </Button>
        <Button href="/family" variant="contained" color="primary">
          Family
        </Button>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            invoke();
            window.location.href = "/";
          }}
        >
          Logout
        </Button>
      </Toolbar>
    </AppBar>
  );
};
export default Header;
