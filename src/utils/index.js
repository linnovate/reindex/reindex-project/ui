export const arrayToObject = array => {
  const obj = {};
  array.map(cell => {
    obj[cell] = null;
  });
  return obj;
};
