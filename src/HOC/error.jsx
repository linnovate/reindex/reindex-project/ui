import React from "react";

export class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error });
    console.log("Some error occourd: ", error, errorInfo);
  }

  render() {
    if (this.state.error) {
      return (
        <div>
          Some errors occourd please inspect console for further information
        </div>
      );
    }
    return this.props.children;
  }
}
