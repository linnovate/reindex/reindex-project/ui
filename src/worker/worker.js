var CACHE_NAME = "cache_name";

self.addEventListener("install", function(event) {
  // triggered on service worker installation, when done load and injected into the client browser
});

self.addEventListener("fetch", function(event) {
  // any client requests gets intercepted by the worker on fetch event
  event.respondWith(fetch(event.request, { credentials: "include" }));
});

self.addEventListener("activate", function(event) {
  // fired when an old service worker get killed and a new service worker starts
});
