FROM node:latest AS builder

ARG K8S_BUILD=""

ARG GRAPH_URL
ARG NODE_ENV
ARG WORKER
ARG PUBLIC_PATH

ENV GRAPH_URL $GRAPH_URL
ENV WORKER $WORKER
ENV NODE_ENV $NODE_ENV
ENV PUBLIC_PATH $PUBLIC_PATH

WORKDIR /usr/src/app

# ENV PATH /app/node_modules/.bin:$PATH

COPY package*.json /usr/src/app/

RUN npm install --silent --quiet

COPY webpack* /usr/src/app/

COPY ./src /usr/src/app/src

RUN ls 

RUN if [ -n "$K8S_BUILD" ] ; then npm run build:production; else npm run dev ; fi

